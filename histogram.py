# -*- coding: utf-8 -*-

import sys
import numpy as np
import pylab
import pyaudio
import audio_utils
from glob import glob
from numpy import linalg
from scipy import signal
from scipy import fftpack
from scikits.audiolab import wavread
from scikits.talkbox import segment_axis
from scikits.talkbox.features import mfcc
from scikits.learn.svm import LinearSVC, SVC

def calc_ceps(frame, nwin=256):
    w = signal.hamming(nwin, sym=0)
    over = nwin - 160
    framed = segment_axis(frame, nwin, over) * w

    aspec = np.abs(fftpack.fft(framed, n=nwin, axis=-1))
    lspec = 20 * np.log10(aspec)
    cepss = np.real(fftpack.ifft(lspec, n=nwin, axis=-1))
    return cepss

if __name__ == '__main__':
    fn = sys.argv[1]
    print fn
    audio, fs, enc = wavread(fn)
    audio = audio_utils.bpf(audio, fs, 2000, 3000)
    n = 256
    # time = np.arange(0, float(n) / fs, 1.0 / fs)
    time = np.arange(0, n)

    center = len(audio) / 2
    cepss = calc_ceps(audio, nwin=n)

    for ceps in cepss:
        pylab.plot(time[0:n / 2], ceps[0:n/2])
        pylab.show()
        print ceps[2] + ceps[3] + ceps[4] + ceps[5] / 4

    print ''
    print reduce(lambda a, x: a + x[2], cepss, 0) / len(cepss)
        
                
    
