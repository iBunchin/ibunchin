# -*- coding: utf-8 -*-
import struct
import wave
from scipy import signal

def bpf(input, fs, l, h):
    nyq = fs / 2.0
    fe1, fe2 = signal.butter(1, [l / nyq, h / nyq], btype='band')
    filtered = signal.lfilter(fe1, fe2, input)

    return filtered


def save(buffer_, fs, n_bit, fn):
    def limit(x):
        if x > 32767:
            return 32767
        if x < -32768:
            return -32768
        return x

    buffer_ = map(limit, buffer_)
    data = struct.pack("h" * len(buffer_), *buffer_)
    wf = wave.open(fn, 'w')
    wf.setnchannels(1)
    wf.setsampwidth(n_bit / 8)
    wf.setframerate(fs)
    wf.writeframes(data)
    wf.close()

