# -*- coding: utf-8 -*-

import pyaudio
import numpy as np
import audio_utils
import random
from glob import glob
from numpy import linalg
from scipy import signal
from scipy import fftpack
from scikits.audiolab import wavread
from scikits.talkbox import segment_axis
from scikits.talkbox.features import mfcc
from scikits.learn.svm import LinearSVC, SVC
from scikits.learn.metrics.metrics import precision_recall_fscore_support
import pickle
from audio import MFCCDMFCCFeatureExtractor

def prepare_features():
    harais, others, hanes = [], [], []
    labels = []
    estimator = None
    extractor = MFCCDMFCCFeatureExtractor(nceps=17)

    for fn in glob('./Samples/Harai/harai_??.wav'):
        print fn
        audio, fs, enc = wavread(fn)
        features = extractor.next(audio)

        for feature in features:
            harais.append(feature)
        extractor.clear_buffer()

    for fn in glob('./Samples/Hane/hane_??.wav'):
        print fn
        audio, fs, enc = wavread(fn)
        features = extractor.next(audio)

        for feature in features:
            hanes.append(feature)
        extractor.clear_buffer()

    for fn in glob('./Samples/Others/others_??.wav'):
        print fn
        audio, fs, enc = wavread(fn)
        features = extractor.next(audio)

        for feature in features:
            others.append(feature)
        extractor.clear_buffer()

    random.shuffle(harais)
    random.shuffle(hanes)
    random.shuffle(others)

    print "Harai:", len(harais)
    print "Hane:", len(hanes)
    print "Others:", len(others)

    return harais, hanes, others


def create_testset(harais, hanes, others):
    harais_dev = harais[:3 * len(harais) / 4]
    harais_test = harais[3 * len(harais) / 4:]

    hanes_dev = hanes[:3 * len(hanes) / 4]
    hanes_test = hanes[3 * len(hanes) / 4:]

    others_dev = others[:3 * len(others) / 4]
    others_test = others[3 * len(others) / 4:]
        
    return (harais_dev, harais_test), (hanes_dev, hanes_test), (others_dev, others_test)

    
if __name__ == '__main__':
    harais, hanes, others = prepare_features()
    (harais_dev, harais_test), (hanes_dev, hanes_test), (others_dev, others_test) = create_testset(harais, hanes, others)

    data = {
        'harais': {'dev': harais_dev, 'test': harais_test},
        'hanes': {'dev': hanes_dev, 'test': hanes_test},
        'others': {'dev': others_dev, 'test': others_test}
    }

    #with open('dataset_17.pickle', 'w') as f:
    #    pickle.dump(data, f)


