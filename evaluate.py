# -*- coding: utf-8 -*-

import pyaudio
import numpy as np
import audio_utils
import random
import pickle
from glob import glob
from numpy import linalg
from scipy import signal
from scipy import fftpack
from scikits.audiolab import wavread
from scikits.talkbox import segment_axis
from scikits.talkbox.features import mfcc
from scikits.learn.svm import LinearSVC, SVC
from scikits.learn.metrics.metrics import precision_recall_fscore_support
from audio import MFCCDMFCCFeatureExtractor

def prepare_features():
    harais, others, hanes = [], [], []
    labels = []
    estimator = None
    extractor = MFCCDMFCCFeatureExtractor()

    for fn in glob('./Samples/Harai/harai_??.wav'):
        print fn
        audio, fs, enc = wavread(fn)
        features = extractor.next(audio)

        for feature in features:
            harais.append(feature)
        extractor.clear_buffer()

    for fn in glob('./Samples/Hane/hane_??.wav'):
        print fn
        audio, fs, enc = wavread(fn)
        features = extractor.next(audio)

        for feature in features:
            hanes.append(feature)
        extractor.clear_buffer()

    for fn in glob('./Samples/Others/others_??.wav'):
        print fn
        audio, fs, enc = wavread(fn)
        features = extractor.next(audio)

        for feature in features:
            others.append(feature)
        extractor.clear_buffer()

    random.shuffle(harais)
    random.shuffle(hanes)
    random.shuffle(others)

    print "Harai:", len(harais)
    print "Hane:", len(hanes)
    print "Others:", len(others)

    return harais, hanes, others


def create_testset(harais, hanes, others):
    harais_dev = harais[:3 * len(harais) / 4]
    harais_test = harais[3 * len(harais) / 4:]

    hanes_dev = hanes[:3 * len(hanes) / 4]
    hanes_test = hanes[3 * len(hanes) / 4:]

    others_dev = others[:3 * len(others) / 4]
    others_test = others[3 * len(others) / 4:]
        
    return (harais_dev, harais_test), (hanes_dev, hanes_test), (others_dev, others_test)


def create_label(harais, hanes, others):
    return [1 for x in harais] + [2 for x in hanes] + [3 for x in others]

    concat = harais + hanes + others
    concat_label = [1 for x in harais] + [2 for x in hanes] + [3 for x in others]


def create_estimator(harais, hanes, others):
    estimator = SVC(degree=2, C=100000, kernel='poly')
    estimator.fit(np.array(harais + hanes + others),
                  np.array(create_label(harais, hanes, others)),
              )

    return estimator

    
if __name__ == '__main__':
    with open('dataset_17.pickle', 'r') as f:
        data = pickle.load(f)

    harais, hanes, others = data['harais']['dev'], data['hanes']['dev'], data['others']['dev']
    estimator = create_estimator(harais, hanes, others)

    harais_test, hanes_test, others_test = data['harais']['test'], data['hanes']['test'], data['others']['test']
    true_label = create_label(harais_test, hanes_test, others_test)
        
    preds = estimator.predict(harais_test + hanes_test + others_test)
    print precision_recall_fscore_support(true_label, preds, labels=[1, 2, 3])

