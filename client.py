import socket
import struct
import datetime

UDP_IP = "0.0.0.0"
UDP_PORT = 10000

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

while True:
    start = datetime.datetime.now()
    data, addr = sock.recvfrom(256)
    end = datetime.datetime.now()
    delta = end - start
    print "received message:", struct.unpack('128H', data)
    # print 1 / (delta.total_seconds() / 256)
