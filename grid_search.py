# -*- coding: utf-8 -*-

import pyaudio
import numpy as np
import audio_utils
import random
import pickle
from glob import glob
from numpy import linalg
from scipy import signal
from scipy import fftpack
from scikits.audiolab import wavread
from scikits.talkbox import segment_axis
from scikits.talkbox.features import mfcc
from scikits.learn.svm import LinearSVC, SVC
from scikits.learn.metrics.metrics import precision_recall_fscore_support
from scikits.learn import grid_search
from audio import MFCCDMFCCFeatureExtractor
from operator import itemgetter

def create_label(harais, hanes, others):
    return np.array([1 for x in harais] + [2 for x in hanes] + [3 for x in others])


def create_estimator(harais, hanes, others):
    estimator = SVC(degree=2, C=400000, kernel='poly')
    estimator.fit(np.array(harais + hanes + others),
                  np.array(create_label(harais, hanes, others)),
              )

    return estimator


def report(grid_scores, n_top=3):
    """top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")"""
    print grid_scores

    
if __name__ == '__main__':
    with open('dataset.pickle', 'r') as f:
        data = pickle.load(f)
    
    harais, hanes, others = data['harais']['dev'], data['hanes']['dev'], data['others']['dev']
    train = harais + hanes + others
    label = create_label(harais, hanes, others)

    # print type(harais)
    param_grid = {
        'C': [100000],
        'degree': [2,3,4,5,6,7,8]
    }

    svc = SVC(kernel='poly')
    estimator = grid_search.GridSearchCV(svc, param_grid, n_jobs=8)
    estimator.fit(train, label)
    report(estimator.grid_scores_)











