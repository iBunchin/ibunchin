# -*- coding: utf-8 -*-

from glob import glob
import numpy as np
from scikits.audiolab import wavread
from scikits.talkbox.features import mfcc
from scikits.learn import svm
from scikits.learn import cross_val

harais, others = [], []
labels = []

def wav_to_mfcc(fn):
    audio, fs, enc = wavread(fn)
    ceps, mspec, spec = mfcc(audio, fs=fs, nceps=13, nwin=256)

    return ceps

if __name__ == '__main__':
    for fn in glob('./Samples/Harai/*_??.wav'):
        print fn
        mfcc_ = wav_to_mfcc(fn)
        for x in mfcc_:
            harais.append(x)

    for fn in glob('./Samples/Others/old/*_??.wav'):
        print fn
        mfcc_ = wav_to_mfcc(fn)
        for x in mfcc_:
            others.append(x)

    harais_learn = harais[0:len(harais) * 3 / 4]
    harais_pred = harais[len(harais) * 3 / 4:]

    others_learn = others[0:len(others) * 3 / 4]
    others_pred = others[len(others) * 3 / 4:]
    
    concat_learn = harais_learn + others_learn
    concat_pred = harais_pred + others_pred
    concat_learn_label = [1 for x in harais_learn] + [0 for x in others_learn]

    estimator = svm.SVC(degree=2, C=400000, kernel='poly')
    #estimator = svm.LinearSVC(C=1)
    estimator.fit(np.array(concat_learn),
                  np.array(concat_learn_label),
    #              class_weight = {0: 0.6, 1: 0.40}
    )

    label_predict = estimator.predict(np.array(concat_pred))

    # result
    success_count = 0
    np.set_printoptions(threshold=np.nan)
    print label_predict
    for x in label_predict[0:len(harais_pred)]:
        if x == 1:
            success_count += 1

    for x in label_predict[len(harais_pred):]:
        if x == 0:
            success_count += 1

    success_ratio = success_count / float(len(label_predict))
    print "success: %d" % success_count
    print "total: %d" % len(label_predict)
    print "success_ratio: %f" % success_ratio
    
