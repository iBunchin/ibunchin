# -*- coding: utf-8 -*-

import numpy as np
import pyaudio

if __name__ == '__main__':
    pa = pyaudio.PyAudio()

    device_count = pa.get_device_count()
    infoList = (pa.get_device_info_by_index(i) for i in xrange(device_count))
    for x in infoList:
        print x['name']
        print x

    stream = pa.open(input_device_index=4,
                     format=pyaudio.paInt16,
                     channels=1,
                     rate=16000,
                     input=True,
                     output=False,
                     frames_per_buffer=512
                 )

    stream2 = pa.open(input_device_index=6,
                     format=pyaudio.paInt16,
                     channels=1,
                     rate=8000,
                     input=True,
                     output=False,
                     frames_per_buffer=512
                 )

    while True:
        avail = stream2.get_read_available()
        if avail:
            print avail
            data = np.frombuffer(stream2.read(avail), dtype="int16") / 32765.0
    
