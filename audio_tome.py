# -*- coding: utf-8 -*-

import pyaudio
import numpy as np
import audio_utils
from glob import glob
from numpy import linalg
from scikits.audiolab import wavread
from scikits.talkbox.features import mfcc
from scikits.learn.svm import LinearSVC, SVC

class FeatureExtractor(object):
    def __init__(self, fs=16000, nwin=256):
        self._fs = fs
        self._nwin = nwin
        pass

    def next(self, frame):
        return None

class MFCCFeatureExtractor(FeatureExtractor):
    def __init__(self, fs=16000, nwin=256, nceps=25):
        super(MFCCFeatureExtractor, self).__init__(fs, nwin)
        self._nceps = nceps

    def next(self, frame):
        # frame = audio_utils.bpf(frame, self._fs, 2000, 4000)
        ceps, mspec, spec = mfcc(frame,
                                 fs=self._fs,
                                 nwin=self._nwin,
                                 nceps=self._nceps)

        return ceps

class DeltaMFCCFeatureExtractor(MFCCFeatureExtractor):
    def __init__(self, fs=16000, nwin=256, nceps=25, ndelta=3):
        print 'ndelta', ndelta
        super(DeltaMFCCFeatureExtractor, self).__init__(fs, nwin)
        self._buffer = []
        self._ndelta = ndelta
        self._nbuffer = ndelta * 2 + 1
        self._x = np.array([[x, 1] for x in xrange(self._nbuffer)])

    def next(self, frame):
        mfccs = super(DeltaMFCCFeatureExtractor, self).next(frame)
        dmfccs = []
        for mfcc in mfccs:
            self._buffer.append(mfcc)
            if len(self._buffer) < self._nbuffer:
                continue

            if len(self._buffer) > self._nbuffer:
                self._buffer.pop(0)
                
            assert(len(self._buffer) == self._nbuffer)
            dmfcc = self._regress()
            dmfccs.append(dmfcc)

        return dmfccs

    def clear_buffer(self):
        self._buffer = []

    def _regress(self):
        slope, intercept = linalg.lstsq(self._x, np.array(self._buffer))[0]
        return slope

class MFCCDMFCCFeatureExtractor(DeltaMFCCFeatureExtractor):
    def next(self, frame):
        mfccs = super(DeltaMFCCFeatureExtractor, self).next(frame)
        features = []
        for mfcc in mfccs:
            self._buffer.append(mfcc)
            if len(self._buffer) < self._nbuffer:
                continue

            if len(self._buffer) > self._nbuffer:
                self._buffer.pop(0)

            center_mfcc = self._buffer[self._ndelta]
                
            assert(len(self._buffer)) == self._nbuffer
            dmfcc = self._regress()
            feature = np.hstack([dmfcc, center_mfcc])
            features.append(feature)

        return features
    
def prepare_estimator():
    harais, others, hanes = [], [], []
    labels = []
    estimator = None
    extractor = MFCCDMFCCFeatureExtractor()

    for fn in glob('./Samples/Harai/harai_??.wav'):
        print fn
        audio, fs, enc = wavread(fn)
        features = extractor.next(audio)
        print len(features)

        for feature in features:
            harais.append(feature)
        #extractor.clear_buffer()

    for fn in glob('./Samples/Hane/hane_??.wav'):
        print fn
        audio, fs, enc = wavread(fn)
        features = extractor.next(audio)
        print len(features)
        for feature in features:
            hanes.append(feature)
        #extractor.clear_buffer()

    for fn in glob('./Samples/Others/others_??.wav'):
        print fn
        audio, fs, enc = wavread(fn)
        features = extractor.next(audio)
        print len(features)
        for feature in features:
            others.append(feature)
        #extractor.clear_buffer()

    concat = harais + hanes + others
    concat_label = [1 for x in harais] + [2 for x in hanes] + [0 for x in others]

    print len(harais), len(hanes), len(others)
    # estimator = LinearSVC(C=1)
    estimator = SVC(degree=4, C=400000, kernel='poly')
    # estimator = SVC(C=40000)
    estimator.fit(np.array(concat),
                  np.array(concat_label),
                  class_weight={0: 1, 1: 1, 2: 1}
              )

    return estimator


if __name__ == '__main__':
    from scikits.audiolab import play
    FS = 16000
    estimator = prepare_estimator()
    pa = pyaudio.PyAudio()
    device_count = pa.get_device_count()
    infoList = (pa.get_device_info_by_index(i) for i in xrange(device_count))
    for x in infoList:
        print x['name']
        print x

    stream = pa.open(input_device_index=4,
                     format=pyaudio.paInt16,
                     channels=1,
                     rate=FS,
                     input=True,
                     output=False,
                 )

    buffers = []
    overlap = 256 - 160
    extractor = MFCCDMFCCFeatureExtractor()
    
    history = [0, 0, 0]
    c = 0
    d = 0
    last_cep = None

    while True:
        try:
            avail = stream.get_read_available()
            if not avail >= 256:
                continue
                
            data = list(np.frombuffer(stream.read(avail), dtype="int16") / 32768.0)
            if len(buffers) < 3:
                buffers.append(data)
                continue

            data_over = buffers[0][-overlap:] + buffers[1] + buffers[2][:overlap]
            buffers.pop(0)
            buffers.append(data)
                
            features = extractor.next(data_over)
            
            preds = estimator.predict(features)
            for pred in preds:
                print pred
                history.pop(0)
                history.append(pred)
                # if len(filter(lambda x: x == 0, history))  <= 1:
                if (0 not in history) and (2 not in history):
                    print c, "Harai!"
                    c += 1
                if (0 not in history) and (1 not in history):
                    print d, "Hane!"
                    d += 1
                    
        except KeyboardInterrupt:
            break
