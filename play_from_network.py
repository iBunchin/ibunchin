import socket
import struct
import datetime
import pyaudio
import numpy as np
import wave
import sys

UDP_IP = "0.0.0.0"
UDP_PORT = 10000

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

pa = pyaudio.PyAudio()
device_count = pa.get_device_count()
infoList = (pa.get_device_info_by_index(i) for i in xrange(device_count))
for x in infoList:
    print x['name']
    print x
        
play_stream = pa.open(output_device_index=1,
                      format=pyaudio.paInt16,
                      channels=1,
                      rate=24000,
                      input=False,
                      output=True,
                      frames_per_buffer=256
                      )

buf = []

while True:
    try:
        data, addr = sock.recvfrom(256*2)
        buf.append(data)
        data = np.frombuffer(data, dtype='int16')
        # print len(data)
        play_stream.write(data)
    except KeyboardInterrupt:
        wf = wave.open("recorded.wav", 'wb')
        wf.setnchannels(1)
        wf.setsampwidth(2)
        wf.setframerate(16000)
        wf.writeframes(b''.join(buf))
        wf.close()
        sys.exit()
        
        

    
    
