# -*- coding: utf-8 -*-

import datetime
import pyaudio
import numpy as np
import audio_utils
import pygame
from glob import glob
from numpy import linalg
from scipy import signal
from scipy import fftpack
from scikits.audiolab import wavread
from scikits.talkbox import segment_axis
from scikits.talkbox.features import mfcc
from scikits.learn.svm import LinearSVC, SVC

pygame.mixer.init()
g_tome = pygame.mixer.Sound("se/tome.wav")
g_hane = pygame.mixer.Sound("se/hane.wav")
g_harai = pygame.mixer.Sound("se/harai.wav")

class FeatureExtractor(object):
    def __init__(self, fs=16000, nwin=256):
        self._fs = fs
        self._nwin = nwin
        pass

    def next(self, frame):
        return None

class CepstrumFeatureExtractor(FeatureExtractor):
    def __init__(self, fs=16000, nwin=256):
        super(CepstrumFeatureExtractor, self).__init__(fs, nwin)
        self._over = self._nwin - 160
        self._w = signal.hamming(self._nwin, sym=0)
    
    def next(self, frame):
        frame = audio_utils.bpf(frame, self._fs, 2000, 3000)
        framed = segment_axis(frame, self._nwin, self._over) * self._w

        aspec = np.abs(fftpack.fft(framed, axis=-1))
        lspec = 20 * np.log10(aspec)
        ceps = np.real(fftpack.ifft(lspec, axis=-1))
        return ceps
        
        
class MFCCFeatureExtractor(FeatureExtractor):
    def __init__(self, fs=16000, nwin=256, nceps=25):
        super(MFCCFeatureExtractor, self).__init__(fs, nwin)
        self._nceps = nceps

    def next(self, frame):
        # frame = audio_utils.bpf(frame, self._fs, 2000, 4000)
        ceps, mspec, spec = mfcc(frame,
                                 fs=self._fs,
                                 nwin=self._nwin,
                                 nceps=self._nceps)

        return ceps

class DeltaMFCCFeatureExtractor(MFCCFeatureExtractor):
    def __init__(self, fs=16000, nwin=256, nceps=25, ndelta=3):
        print 'ndelta', ndelta
        super(DeltaMFCCFeatureExtractor, self).__init__(fs, nwin)
        self._buffer = []
        self._ndelta = ndelta
        self._nbuffer = ndelta * 2 + 1
        self._x = np.array([[x, 1] for x in xrange(self._nbuffer)])

    def next(self, frame):
        mfccs = super(DeltaMFCCFeatureExtractor, self).next(frame)
        dmfccs = []
        for mfcc in mfccs:
            self._buffer.append(mfcc)
            if len(self._buffer) < self._nbuffer:
                continue

            if len(self._buffer) > self._nbuffer:
                self._buffer.pop(0)
                
            assert(len(self._buffer) == self._nbuffer)
            dmfcc = self._regress()
            dmfccs.append(dmfcc)

        return dmfccs

    def clear_buffer(self):
        self._buffer = []

    def _regress(self):
        slope, intercept = linalg.lstsq(self._x, np.array(self._buffer))[0]
        return slope

class MFCCDMFCCFeatureExtractor(DeltaMFCCFeatureExtractor):
    def next(self, frame):
        mfccs = super(DeltaMFCCFeatureExtractor, self).next(frame)
        features = []
        for mfcc in mfccs:
            self._buffer.append(mfcc)
            if len(self._buffer) < self._nbuffer:
                continue

            if len(self._buffer) > self._nbuffer:
                self._buffer.pop(0)

            center_mfcc = self._buffer[self._ndelta]
                
            assert(len(self._buffer)) == self._nbuffer
            dmfcc = self._regress()
            feature = np.hstack([dmfcc, center_mfcc])
            features.append(feature)

        return features


class HaneHaraiDetector(object):
    def __init__(self, estimator):
        self._estimator = estimator
        self._state = 0
        self._buffer = []

    def next(self, frame):
        estimated = self._estimator.predict(frame)[0]
        self._buffer.append(estimated)

        if len(self._buffer) > 3:
            self._buffer.pop(0)

        if self._buffer[1:] == [2, 2]:
            self._buffer = []
            self._state = 2

        elif self._buffer[1:] == [1, 1]:
            self._buffer = []
            self._state = 1

        else:
            self._state = 0

        return self._state

    def get_state(self):
        return self._state


class WritingStateRecognizer(object):
    def __init__(self):
        self._state = 0
        self._buffer = []

    def next(self, frame):
        frame_avg = sum((abs(x) for x in frame)) / len(frame)
        self._buffer.append(1 if frame_avg > 0.1 else 0)

        if len(self._buffer) > 2:
            self._buffer.pop(0)

        if self._buffer == [1, 1]:
            self._state = 1

        elif self._buffer == [0, 0]:
            self._state = 0

        return self._state

    def get_state(self):
        return self._state


class TomeDetector(object):
    def __init__(self, wsr):
        self._wsr = wsr
        self._prev = 0
        self._state = 0

    def next(self, frame):
        ws = self._wsr.next(frame)
        if self._prev == 1 and ws == 0:
            self._state = 1
        else:
            self._state = 0

        self._prev = ws
        return self._state

    def get_state(self):
        return self._state

class WritingActionDetector(object):
    def __init__(self, hane_harai_detector, writing_state_detector, tome_detector):
        self._hane_harai_detector = hane_harai_detector
        self._tome_detector = tome_detector
        self._writing_state_detector = writing_state_detector
        self._last_fire = datetime.datetime.now()

    def next(self, frame):
        self._update(frame)
        if self._writing_state_detector.get_state() == 1:
            hane_or_harai = self._hane_harai_detector.get_state()
            if hane_or_harai == 1:
                self.try_fire(1)
                return
            elif hane_or_harai == 2:
                self.try_fire(2)
                return
        
        tome = self._tome_detector.get_state()
        if tome == 1:
            self.try_fire(0)

    def _update(self, frame):
        self._hane_harai_detector.next(frame)
        self._tome_detector.next(frame)
        self._writing_state_detector.next(frame)
        
    def try_fire(self, action):
        now = datetime.datetime.now()
        td = now - self._last_fire
        if td.total_seconds() < 0.2:
            return

        if action == 0:
            print "Tome"
            g_tome.play()
        elif action == 1:
            print "Harai"
            g_harai.play()
        elif action == 2:
            print "Hane"
            g_hane.play()

        self._last_fire = datetime.datetime.now()


class Estimator(object):
    def __init__(self, feature_extractor):
        self._estimator = None
        self._extractor = feature_extractor
        self._prepare_estimator()

    def _prepare_estimator(self):
        harais, others, hanes = [], [], []
        labels = []
        estimator = None

        for fn in glob('./Samples/WebCam/Harai/harai_??.wav'):
            print fn
            audio, fs, enc = wavread(fn)
            features = self._extractor.next(audio)
            print len(features)

            for feature in features:
                harais.append(feature)
                #extractor.clear_buffer()

        for fn in glob('./Samples/WebCam/Hane/hane_??.wav'):
            print fn
            audio, fs, enc = wavread(fn)
            features = self._extractor.next(audio)
            print len(features)
            for feature in features:
                hanes.append(feature)
                #extractor.clear_buffer()

        for fn in glob('./Samples/WebCam/Others/others_??.wav'):
            print fn
            audio, fs, enc = wavread(fn)
            features = self._extractor.next(audio)
            print len(features)
            for feature in features:
                others.append(feature)
                #extractor.clear_buffer()

        concat = harais + hanes + others
        concat_label = [1 for x in harais] + [2 for x in hanes] + [0 for x in others]

        print len(harais), len(hanes), len(others)
        # estimator = LinearSVC(C=1)
        estimator = SVC(degree=2, C=100000, kernel='poly')
        # estimator = SVC(C=40000)
        estimator.fit(np.array(concat),
                      np.array(concat_label),
                      class_weight={0: 1, 1: 2, 2: 1}
                  )

        self._estimator = estimator

    def predict(self, frame):
        feature = self._extractor.next(frame)
        return self._estimator.predict(feature)


if __name__ == '__main__':
    FS = 16000
    estimator = Estimator(MFCCDMFCCFeatureExtractor())
    pa = pyaudio.PyAudio()
    device_count = pa.get_device_count()
    infoList = (pa.get_device_info_by_index(i) for i in xrange(device_count))
    for x in infoList:
        print x['name']
        print x

    stream = pa.open(input_device_index=4,
                     format=pyaudio.paInt16,
                     channels=1,
                     rate=FS,
                     input=True,
                     output=False,
                 )

    buffers = []
    overlap = 256 - 160
    recognizer = WritingActionDetector(HaneHaraiDetector(estimator), WritingStateRecognizer(), TomeDetector(WritingStateRecognizer()))

    while True:
        try:
            avail = stream.get_read_available()
            if not avail >= 256:
                continue
                
            # print list(np.frombuffer(stream.read(avail), dtype="int16"))
            data = list(np.frombuffer(stream.read(256), dtype="int16") / 32768.0)
            # print data
            # if len(buffers) < 3:
            #     buffers.append(data)
            #     continue

            # data_over = buffers[0][-overlap:] + buffers[1] + buffers[2][:overlap]
            # buffers.pop(0)
            # buffers.append(data)
            # print len(data_over)
            # print len(data)
            recognizer.next(data)
                    
        except KeyboardInterrupt:
            break
